<?php

function upload_multiple_images($images_array,$images_tmp,$images_size,$dir){
		$res=array();

		// extension array
		$ext=array('png','jpg','gif','jpeg','bmp');

		// Define size of image in kb
		$size=2000;

		// Count images
		$total_images=count($images_array);
		$res['total_uploaded_images']=$total_images;

		// check directory exist or not
		if(file_exists($dir)){
			// loop over image
			$count=0;
			foreach($images_array as $single_image){
				// Get image extension
				$image_ext=pathinfo($single_image,PATHINFO_EXTENSION);

				// Check image is valid or not
				if(in_array($image_ext,$ext)){
					// check size of each image
					$image_size=round($images_size[$count]/1000);
					if($image_size>$size){
						$res[$single_image][]='This has exceed limit of maximum size.';
					}else{
						$res['filesize'][]=$image_size;
						echo $image_size;
						// rename image if exist else upload in folder
						if(file_exists($dir.'/'.$single_image)){
							// create random number
							$rand_num=rand(101,99999);
							$single_image=$rand_num.'_'.$single_image;
							// upload file on server
							move_uploaded_file($images_tmp[$count],$dir.'/img'.$single_image);
							$res[$single_image][]='file has been uploaded';
						}else{
							// upload file on server
							move_uploaded_file($images_tmp[$count],$dir.'/'.$single_image);
							$res[$single_image][]='file has been uploaded';
						}
					}
				}else{
					$res['msg']='Image is not valid.';
				}
				$count++;
			}
		}else{
			$res['msg']=$dir.' directory doesn\'t exist.';
		}

		return $res;
	}
?>
