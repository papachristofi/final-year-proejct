
<?php
include 'core/init.php';
protect_page();

if(empty($_POST) ===false){
	$required_fields = array('current_password','password','password_again');
	foreach($_POST as $key=>$value){
		if(empty($value) && in_array($key,$required_fields) ===true){
			$errors[] = 'Fields marked with an asterisk are required';
			break 1;
		}
	}
	//compare encrypted password with user password
	if(md5($_POST['current_password']) === $user_data['password']){
		if(trim($_POST['password']) !== trim($_POST['password_again'])){
			$errors[] = 'Your New passwords do not match';
		}
		else if(strlen($_POST['password'])<6){
			$errors[] = 'Your passwords must be at least 6 characters';
		}
	}
	else{
		$errors[] = 'Your current password is incorrect';
	}

	//echo $user_data['password'];
	//print_r($errors);

}

include 'includes/overall/overall_header.php';?>
<?php
if(isset($_GET['success']) && empty($_GET['success'])){
	echo '<h3>You have Changed Your Password Successfully!</h3>';
}
else{
	if(empty($_POST) ===false && empty($errors)===true){
		change_password($session_user_id,$_POST['password']);
		echo '<script>window.location = "'.'changepassword.php?success'.'";</script>';


	}
	else if(empty($errors)===false){
		echo "<h3>" .output_errors($errors). "</h3>";
	}

	?>

	<div class='col-sm-4 col-md-offset-4'>
		<h1>Change Password</h1>
		<br>
          <form accept-charset="UTF-8" action="changepassword.php" class="require-validation" data-cc-on-file="false" data-stripe-publishable-key="pk_bQQaTxnaZlzv4FnnuZ28LFHccVSaj" id="payment-form" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓" /><input name="_method" type="hidden" value="PUT" /><input name="authenticity_token" type="hidden" value="qLZ9cScer7ZxqulsUWazw4x3cSEzv899SP/7ThPCOV8=" /></div>
            <div class='form-row'>
              <div class='col-xs-12 form-group required'>
                <label class='control-label'>Current password *</label>
                <input class='form-control' size='4' type='password' name="current_password">
              </div>
            </div>
            <div class='form-row'>
              <div class='col-xs-12 form-group card required'>
                <label class='control-label'>New password *</label>
                <input autocomplete='off' class='form-control card-number' size='20' type='password' name="password">
              </div>
            </div>
             <div class='form-row'>
              <div class='col-xs-12 form-group card required'>
                <label class='control-label'>Repeate password *</label>
                <input autocomplete='off' class='form-control' size='20' type='password' name="password_again">
              </div>
            </div>

            <div class='form-row'>
              <div class='col-md-12 form-group'>
                <button class='form-control btn btn-primary submit-button' type='submit'>Change password</button>
              </div>
            </div>
          </form>
        </div>




<?php
}
include 'includes/overall/overall_footer.php';?>
