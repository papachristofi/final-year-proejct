
<?php
include 'core/init.php';
protect_page();
admin_protect();
include 'includes/overall/overallheader_admin.php';?>
<div class="page-header">
  <h1>Admin Dashboard</h1>
</div>
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <?php
                            $sql3 = "SELECT count(user_id) FROM users";
                            $results3 = mysql_query($sql3);
                            $row2= mysql_fetch_row($results3);
                            $re = $row2[0];
                            echo $re;
                            ?>
                        </div>
                        <div>Number of Users</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <a href="users_db.php"><span class="pull-left">View Details</span></a>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge">
                          <?php
                          function getUsersOnline() {
                            $count = 0;

                            $handle = opendir(session_save_path());
                            if ($handle == false) return -1;

                            while (($file = readdir($handle)) != false) {
                                if (ereg("^sess", $file)) $count++;
                            }
                            closedir($handle);

                            return $count;
                          }
                          $usercount = getUsersOnline();
                          echo $usercount;
                           ?>
                      </div>
                      <div>Number of Visitors</div>
                  </div>
              </div>
          </div>
          <a href="#">
              <div class="panel-footer">
                  <a href="#"> <span class="pull-left">View Details</span></a>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="row">

<div class="list-group">
  <a href="rename_course.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Rename Course</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit - Rename Main Course Names</p>
  </a>
</div>
<div class="list-group">
  <a href="users_db.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Users Database</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit - Users Database Table</p>
  </a>
</div>
<div class="list-group">
  <a href="rename_system.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Rename System Name</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit - Rename System's Name</p>
  </a>
</div>
</div>
<br>
<br>
<br>
<br>
<br>
<?php   include 'includes/overall/overall_footer.php';?>
