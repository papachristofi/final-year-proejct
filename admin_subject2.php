
<?php
include 'core/init.php';
protect_page();
admin_protect();
include 'includes/overall/overallheader_admin.php';?>

<h1>Subject2 Admin Page</h1>
<ol class="breadcrumb">
  <li><a href="admin.php">Admin</a></li>
  <li class="active">Subject2 Admin Page</li>
</ol>

<div class="list-group">
  <a href="subject2_home_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 2 Home</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 2 Home Details</p>
  </a>
</div>
<div class="list-group">
  <a href="subject1_content2_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 2 Data 1</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 2 Data 1 Details</p>
  </a>
</div>
<div class="list-group">
  <a href="subject2_content2_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 2 Data 2</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 2 Data 2 Details</p>
  </a>
</div>
<div class="list-group">
  <a href="subject2_content3_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 2 Data 3</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 2 Data 3 Details</p>
  </a>

</div>
<div class="list-group">
  <a href="subject2_content4_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 2 Data 4</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 2 Data 4 Details</p>
  </a>

</div>
<div class="list-group">
  <a href="subject2_content5_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 2 Data 5</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 2 Data 5 Details</p>
  </a>
</div>
<div class="list-group">
  <a href="quiz2_questions.php" class="list-group-item secondary">
    <h4 class="list-group-item-heading">Subject 2 Quiz</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 2 Quiz Database</p>
  </a>

</div>
<div class="list-group">
  <a href="rename_submenu2.php" class="list-group-item secondary">
    <h4 class="list-group-item-heading">Rename Submenu 2</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 2 Submenu</p>
  </a>

</div>
<br><br><br>


<?php  include 'includes/overall/overall_footer.php';?>
