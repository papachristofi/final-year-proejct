
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <?php  include 'includes/head.php';?>

    <!-- Custom CSS -->
    <link href="css/simple-sidebar.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="home.php">
                      <?php include'includes/brand_name.php' ?>
                    </a>
                </li>
                <?php include'includes/menu.php' ?>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">

            <div class="container-fluid">
<div class='row header1dd' style="margin-top:-40px;">
        <div class="col-xs-12 col-sm-12 col-md-12">

        <div class="container">

              <a href="#menu-toggle"  id="menu-toggle" >
                <button class="btn btn-inverse btn-large active" style="margin: 0px 0px -60px -45px;">
                  <span class="glyphicon glyphicon-list-alt">
                  </span>
                </button>
              </a>



        <nav class="navbar navbar-inverse " role="navigation">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="home.php">Code Learning</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav llmenu2">
                        <?php
                        $sql3 = "SELECT count(id) FROM course";
                        $results3 = mysql_query($sql3);
                        $row2= mysql_fetch_array($results3);
                        $count = $row2[0];

                        for ($i=1; $i <= $count ; $i++) {
                            $sql = "SELECT * FROM course WHERE id=$i";
                            $results = mysql_query($sql);
                            while ($row= mysql_fetch_row($results)) {

                        $field1 =($row[0]);
                        $field2 =htmlentities($row[1]);
                        $field3 = ($row[2]);
                        echo "<li>";
                        echo "<a href='$field3'>$field2</a>";
                        echo"</li>";
                    }
                }
                        ?>



              </ul>

              <ul class="nav navbar-nav navbar-right">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Login/Register</button>

                                  <!-- Modal -->
                                  <div class="modal fade" id="myModal" role="dialog">
                                    <div id="login-overlay" class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>

                                              <h4 class="modal-title" id="myModalLabel">Login to <?php include 'includes/brand_name.php';?></h4>
                                          </div>
                                          <div class="modal-body">
                                              <div class="row">

                                                  <div class="col-sm-12 col-xs-12">

                                                                            <?php
                                                                            if (logged_in() === true){
                                                                                echo '<div class="well">';
                                                                                include 'includes/widgets/loggedin.php';
                                                                                if (is_admin($session_user_id)==true) {
                                                                                    include'includes/widgets/logged_in_admin.php';


                                                                                    echo "</div>";
                                                                                }
                                                                            }
                                                                            else {
                                                                                ?>
                                                                                <div class="well">
                                                                                        <form id="loginForm" method="POST" action="login.php" novalidate="novalidate">
                                                                                                <div class="form-group">
                                                                                                        <label for="username" class="control-label">Username</label>
                                                                                                        <input type="text" class="form-control" id="username" name="username" value="" required="" title="Please enter you username" placeholder="Username">
                                                                                                        <span class="help-block"></span>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                        <label for="password" class="control-label">Password</label>
                                                                                                        <input type="password" class="form-control" id="password" name="password" value="" required="" title="Please enter your password" placeholder="Password">
                                                                                                        <span class="help-block"></span>
                                                                                                </div>
                                                                                                <div id="loginErrorMsg" class="alert alert-error hide">Wrong username og password</div>
                                                                                                <div class="checkbox">
                                                                                                        <label>
                                                                                                                <input type="checkbox" name="remember" id="remember"> Remember login
                                                                                                        </label>
                                                                                                        <p class="help-block">(if this is a private computer)</p>
                                                                                                </div>
                                                                                                <button type="submit" class="btn btn-success btn-block">Login</button>
                                                                                        </form>
                                                                                </div>
                                                                                <div class="well">
                                                                                        <p class="lead">Register now for <span class="text-success">FREE</span></p>
                                                                                        <ul class="list-unstyled" style="line-height: 2">
                                                                                                <li><span class="fa fa-check text-success"></span> Find Useful Information</li>
                                                                                                <li><span class="fa fa-check text-success"></span> Subjects for Free</li>
                                                                                                <li><span class="fa fa-check text-success"></span> Fast Navigation</li>
                                                                                                <li><span class="fa fa-check text-success"></span> Free Registration</li>
                                                                                                <li><span class="fa fa-check text-success"></span> Access Courses <small>(only when logged in)</small></li>
                                                                                        </ul>
                                                                                        <p><a href="register.php" class="btn btn-info btn-block">Register now!</a></p>
                                                                                </div>
                                                                                <?php
                                                                            }
                                                                            ?>


                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                </div><!-- /.navbar-collapse -->

                  </ul>
                </div><!-- /.container-fluid -->
            </div>
        </nav>



    </div><!-- /container-->
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->

            <div class="row">
                <!-- <div class=" .col-xs-4 sidebar">
            <div class="mini-submenu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </div>
            <div class="list-group">
                <span href="home.php" class="list-group-item active">
                    Code Learning
                    <span class="pull-right" id="slide-submenu">
                        <i class="fa fa-times"></i>
                    </span>
                </span>

            </div> -->
        <div class=".col-xs-12 antonis">

                <div id="container">



        <!-- /#page-content-wrapper -->

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>
