-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 13, 2016 at 06:25 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phplogin`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL,
  `admin_menu_name` text NOT NULL,
  `admin_menu_href` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `admin_menu_name`, `admin_menu_href`) VALUES
(1, 'Rename Course', 'rename_course.php'),
(2, 'Subject 1', 'admin_subject1.php'),
(3, 'Subject 2', 'admin_subject2.php'),
(4, 'Subject 3', 'admin_subject3.php'),
(5, 'Subject 4', 'admin_subject4.php'),
(6, 'Subject 5', 'admin_subject5.php');

-- --------------------------------------------------------

--
-- Table structure for table `answers_subject1`
--

CREATE TABLE `answers_subject1` (
  `q_id` int(11) NOT NULL,
  `q_answer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers_subject1`
--

INSERT INTO `answers_subject1` (`q_id`, `q_answer`) VALUES
(1, 'Hyper-Text Markup Language '),
(2, 'cascading style sheet'),
(3, '<strong></strong>'),
(4, '<strong></strong>'),
(5, ' <body style="background-color:yellow;">');

-- --------------------------------------------------------

--
-- Table structure for table `answers_subject2`
--

CREATE TABLE `answers_subject2` (
  `q_id` int(11) NOT NULL,
  `q_answer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers_subject2`
--

INSERT INTO `answers_subject2` (`q_id`, `q_answer`) VALUES
(1, 'Cascading Style Sheet'),
(2, '<style=background-color:#fff>');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `course_name` text NOT NULL,
  `course_href` varchar(50) NOT NULL,
  `course_menu` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `course_name`, `course_href`, `course_menu`) VALUES
(1, 'HTML', 'html.php', 'html_submenu.php'),
(2, 'CSS', 'css.php', 'css_menu.php'),
(3, 'JAVASCRIPT', 'js_home.php', 'subject3_submenu.php'),
(4, 'PHP', 'php_course.php', 'subject4_submenu.php'),
(5, 'SQL', 'sqlcourse.php', 'subject5_submenu.php');

-- --------------------------------------------------------

--
-- Table structure for table `css_menu`
--

CREATE TABLE `css_menu` (
  `id` int(11) NOT NULL,
  `css_menu_name` text NOT NULL,
  `css_menu_href` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `css_menu`
--

INSERT INTO `css_menu` (`id`, `css_menu_name`, `css_menu_href`) VALUES
(1, 'CSS Home', 'css.php'),
(2, 'CSS Selectors', '#');

-- --------------------------------------------------------

--
-- Table structure for table `main_menu`
--

CREATE TABLE `main_menu` (
  `id` int(11) NOT NULL,
  `main_name` text NOT NULL,
  `main_href` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_menu`
--

INSERT INTO `main_menu` (`id`, `main_name`, `main_href`) VALUES
(1, 'HOME', 'home.php'),
(2, 'Courses', 'courses.php'),
(3, 'Forum', 'forum.php'),
(4, 'Contact Details', 'contact.php');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `q_id` int(11) NOT NULL,
  `q_name` varchar(200) NOT NULL,
  `q_ans_1` varchar(50) NOT NULL,
  `q_ans_2` varchar(50) NOT NULL,
  `q_ans_3` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`q_id`, `q_name`, `q_ans_1`, `q_ans_2`, `q_ans_3`) VALUES
(1, 'What Does Html stands for?', 'Hyper-Text Markup Language ', 'something', 'something'),
(2, 'What CSS stands for?', 'cascading style sheet', 'css', 'dsdsds'),
(3, 'What tag is used to show bold text?', '<strong></strong>', '<b>', '<bold>'),
(4, 'What tag is used to show bold text?', '<strong></strong>', '<b>', '<bold>'),
(5, 'What is the correct HTML for adding a background colour?', ' <background>yellow</background>', ' <body bg="yellow">', ' <body style="background-color:yellow;">'),
(6, 'Choose the correct HTML element to define important text\r\n', '<b>', '<i>', '<strong>');

-- --------------------------------------------------------

--
-- Table structure for table `questions2`
--

CREATE TABLE `questions2` (
  `q_id` int(11) NOT NULL,
  `q_name` varchar(200) NOT NULL,
  `q_ans_1` varchar(50) NOT NULL,
  `q_ans_2` varchar(50) NOT NULL,
  `q_ans_3` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions2`
--

INSERT INTO `questions2` (`q_id`, `q_name`, `q_ans_1`, `q_ans_2`, `q_ans_3`) VALUES
(1, 'What CSS stands for', 'Cascading Style Sheet', 'css', 'sddsdsdsdsd'),
(2, 'What is the correct element to add background colour?', '<style=background-color:#fff>', 'background color', '<background-color>');

-- --------------------------------------------------------

--
-- Table structure for table `subject1_elements`
--

CREATE TABLE `subject1_elements` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject1_elements`
--

INSERT INTO `subject1_elements` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'Html Attribute', 'HTML elements can have attributes\r\nAttributes provide additional information about an element\r\nAttributes are always specified in the start tag\r\nAttributes come in name/value pairs like: name="value"\r\n\r\nThe lang Attribute\r\nThe document language can be declared in the <html> tag.\r\n\r\nThe language is declared in the lang attribute.\r\n\r\nDeclaring a language is important for accessibility applications (screen readers) and search engines:', 'html_lang_attr.png'),
(2, 'The title Attribute', 'HTML paragraphs are defined with the <p> tag.\r\n\r\nIn this example, the <p> element has a title attribute. The value of the attribute is "About W3Schools":', 'html_p_title.png'),
(3, 'The href Attribute', 'HTML links are defined with the <a> tag. The link address is specified in the href attribute:\r\n', 'html_link.png'),
(4, 'Size Attribute', 'HTML images are defined with the <img> tag.\r\n\r\nThe filename of the source (src), and the size of the image (width and height) are all provided as attributes:', 'html_images.png'),
(5, 'The alt Attribute', 'The alt attribute specifies an alternative text to be used, when an HTML element cannot be displayed.\r\n\r\nThe value of the attribute can be read by "screen readers". This way, someone "listening" to the webpage, i.e. a blind person, can "hear" the element.', 'html_images.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject1_elements2`
--

CREATE TABLE `subject1_elements2` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` longtext NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject1_elements2`
--

INSERT INTO `subject1_elements2` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'HTML Styles', 'Setting the style of an HTML element, can be done with the style attribute.\r\n\r\nThe HTML style attribute has the following syntax:\r\n\r\nstyle="property:value;"\r\n\r\nThe property is a CSS property. The value is a CSS value.\r\nNote	You will learn more about CSS later.\r\n', 'html_style.png'),
(2, 'HTML Background Color', 'The background-color property defines the background color for an HTML element:\r\n\r\nThis example sets the background for a page to lightgrey:', 'html_back-color.png'),
(3, 'HTML Text Color', 'The color property defines the text color for an HTML element:\r\n\r\n', 'html_text_color.png'),
(4, 'HTML Fonts', 'The font-family property defines the font to be used for an HTML element:\r\n', 'html_font.png'),
(5, 'HTML Text Size', 'The font-size property defines the text size for an HTML element:\r\n', 'html_text_size.png'),
(6, 'HTML Text Alignment', 'The text-align property defines the horizontal text alignment for an HTML element:\r\n', 'html_text_allignment.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject1_elements3`
--

CREATE TABLE `subject1_elements3` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject1_elements3`
--

INSERT INTO `subject1_elements3` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'HTML Text Formatting Elements', 'HTML Formatting Elements\r\nIn the previous chapter, you learned about HTML styling, using the HTML style attribute.\r\n\r\nHTML also defines special elements for defining text with a special meaning.\r\n\r\nHTML uses elements like  and  for formatting output, like bold or italic text.\r\n\r\nFormatting elements were designed to display special types of text:\r\n\r\n-Bold text\r\n-Important text\r\n-Italic text\r\n-Emphasized text\r\n-Marked text\r\n-Small text\r\n-Deleted text\r\n-Inserted text\r\n-Subscripts\r\n-Superscripts', 'html_formatting.png'),
(2, 'HTML Bold and Strong Formatting', 'The HTML  element defines bold text, without any extra importance.\r\n', 'html_bold.png'),
(3, 'HTML Italic and Emphasized Formatting', 'The HTML  element defines italic text, without any extra importance.\r\n\r\nThe HTML  element defines emphasized text, with added semantic importance.\r\n', 'html_italic.png'),
(4, 'HTML Small Formatting', 'The HTML  element defines small text:\r\n', 'html_small_formatting.png'),
(5, 'HTML Marked Formatting', 'The HTML  element defines marked or highlighted text:\r\n', 'html_marked.png'),
(6, 'HTML Deleted Formatting', 'The HTML  element defines deleted (removed) text.\r\n', 'html_del.png'),
(7, 'HTML Inserted Formatting', 'The HTML  element defines inserted (added) text.\r\n', 'html_ins.png'),
(8, 'HTML Subscript Formatting', 'The HTML  element defines subscripted text.\r\n', 'html_sub.png'),
(9, 'HTML Superscript Formatting', 'The HTML  element defines superscripted text.\r\n', 'html_sup.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject1_elements4`
--

CREATE TABLE `subject1_elements4` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(30000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject1_elements4`
--

INSERT INTO `subject1_elements4` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'Color Names', 'With CSS, colors can be set by using color names:\r\n', 'html_color.png'),
(2, 'RGB (Red, Green, Blue)', 'With HTML, RGB color values can be specified using this formula: rgb(red, green, blue)\r\n\r\nEach parameter (red, green, and blue) defines the intensity of the color between 0 and 255.\r\n\r\nFor example, rgb(255,0,0) is displayed as red, because red is set to its highest value (255) and the others are set to 0. Experiment by mixing the RGB values below:', 'html_rgb_color.png'),
(3, 'Hexadecimal Colors', 'With HTML, RGB values can also be specified using hexadecimal color values in the form: #RRGGBB, where RR (red), GG (green) and BB (blue) are hexadecimal values between 00 and FF (same as decimal 0-255).\r\n\r\nFor example, #FF0000 is displayed as red, because red is set to its highest value (FF) and the others are set to the lowest value (00).', 'html_hex_color.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject1_elements5`
--

CREATE TABLE `subject1_elements5` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `video_src` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject1_elements5`
--

INSERT INTO `subject1_elements5` (`id`, `name`, `value`, `video_src`) VALUES
(1, 'HTML Tutorial Part 1', 'This video describes HTML tags, elements, and attributes. The new HTML5 doctype declaration is shown as well as the method for specifying the character encoding for the document. The basic structure of an HTML document is also shown. This video introduces the html, head, meta, title, body, and p tags.', 'https://www.youtube-nocookie.com/embed/9gTw2EDkaDQ'),
(2, 'HTML Tutorial Part 2', 'This video describes how to use text. The p tag is demonstrated and the heading tags are introduced. The br tag is also shown. This video also touches on the importance of CSS. A quick demonstration of CSS is shown to center some text and to change its color. In older versions of HTML, text could be centered by using the align attribute. However, this attribute is deprecated and is not valid HTML5. While the align attribute may still work in your browser, it is not compliant with HTML5 standards. Therefore, learning CSS can be very beneficial when working with HTML5 code.', 'https://www.youtube-nocookie.com/embed/YcApt9RgiT0'),
(3, 'HTML Tutorial Part 3', 'This video demonstrates how to use images and hyperlinks. The difference between relative and absolute addressing is covered. This video shows the height and width attributes used with the img tag. In older versions of HTML, the height and width attributes could be specified in pixels or as a percentage. However, it is not valid HTML5 to specify these values as a percentage. If a percentage value is desired then CSS can be used.', 'https://www.youtube-nocookie.com/embed/CGSdK7FI9MY'),
(4, 'HTML Tutorial Part 4', 'This video introduces 2 elements that are new to HTML5, the audio and video elements. While older browsers don''t support these new elements, the popular newer browsers do.', 'https://www.youtube-nocookie.com/embed/4I1WgJz_lmA'),
(5, 'HTML Tutorial 5', 'This tutorial will introduce 7 elements that are new to HTML5. These elements will be used to build a sample web page. The new elements covered in this video are: article, aside, footer, header, main, nav, and section. ', 'https://www.youtube-nocookie.com/embed/dDn9uw7N9Xg'),
(6, 'HTML Tutorial 6', 'This tutorial will use CSS to style and control the layout for the HTML5 code that was developed in the previous video. This tutorial does not cover the basics of CSS but instead it is intended to demonstrate how HTML and CSS work together.', 'https://www.youtube-nocookie.com/embed/CPcS4HtrUEU');

-- --------------------------------------------------------

--
-- Table structure for table `subject1_home`
--

CREATE TABLE `subject1_home` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` longtext NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject1_home`
--

INSERT INTO `subject1_home` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'HTML Introduction', 'HTML is a markup language for describing web documents (web pages).\n\nHTML stands for Hyper Text Markup Language\nA markup language is a set of markup tags\nHTML documents are described by HTML tags\nEach HTML tag describes different document content                                                   ', 'html_home.png'),
(2, 'HTML Page Structure', '<html>\r\n<head>\r\n<title>Page title</title>\r\n</head>\r\n<body>\r\n<h1>This is a heading</h1>\r\n<p>This is a paragraph.</p>\r\n<p>This is another paragraph.</p>\r\n</body>\r\n</html>\r\n\r\n\r\nThe <!DOCTYPE> Declaration\r\nThe <!DOCTYPE> declaration helps the browser to display a web page correctly.\r\n\r\nThere are different document types on the web.\r\n\r\nTo display a document correctly, the browser must know both type and version.\r\n\r\nThe doctype declaration is not case sensitive. All cases are acceptable:', 'doc_type.png'),
(3, 'Write HTML Using Notepad or TextEdit', 'HTML can be edited by using professional HTML editors like:\r\n\r\nMicrosoft WebMatrix\r\nSublime Text\r\nHowever, for learning HTML we recommend a text editor like Notepad (PC) or TextEdit (Mac).\r\n\r\nWe believe using a simple text editor is a good way to learn HTML.\r\n\r\nFollow the 4 steps below to create your first web page with Notepad.\r\n\r\nStep 1: Open Notepad\r\nTo open Notepad in Windows 7 or earlier:\r\n\r\nClick Start (bottom left on your screen). Click All Programs. Click Accessories. Click Notepad.\r\n\r\nTo open Notepad in Windows 8 or later:\r\n\r\nOpen the Start Screen (the window symbol at the bottom left on your screen). Type Notepad.\r\n\r\nStep 2: Write Some HTML\r\nWrite or copy some HTML into Notepad. \r\n\r\nStep 3: Save the HTML Page\r\nSave the file on your computer.\r\n\r\nSelect File > Save as in the Notepad menu.\r\n\r\nName the file "index.html" or any other name ending with html or htm.\r\n\r\nUTF-8 is the preferred encoding for HTML files.\r\n\r\nANSI encoding covers US and Western European characters only.\r\n\r\nStep 4: View HTML Page in Your Browser\r\nOpen the saved HTML file in your favorite browser. The result will look much like this:', 'html_editor.png'),
(4, 'Html Documents', 'All HTML documents must start with a type declaration: <!DOCTYPE html>.\r\n\r\nThe HTML document itself begins with <html> and ends with </html>.\r\n\r\nThe visible part of the HTML document is between <body> and </body>.\r\n', 'html_home.png'),
(5, 'HTML Headings', 'HTML headings are defined with the <h1> to <h6> tags:\r\n', 'html_heading.png'),
(6, 'HTML Paragraphs', 'HTML paragraphs are defined with the <p> tag:\r\n', 'html_paragraph.png'),
(7, 'HTML Links', 'HTML links are defined with the <a> tag:\r\n', 'html_link.png'),
(8, 'HTML Images', '\r\nHTML images are defined with the <img> tag.\r\n\r\nThe source file (src), alternative text (alt), and size (width and height) are provided as attributes:', 'html_images.png'),
(9, 'HTML Elements', 'HTML elements are written with a start tag, with an end tag, with the content in between:\r\n\r\n<tagname>content</tagname>\r\nThe HTML element is everything from the start tag to the end tag:\r\n\r\n<p>My first HTML paragraph.</p>', 'html_elements.png'),
(10, 'Nested HTML Elements', 'HTML elements can be nested (elements can contain elements).\r\n\r\nAll HTML documents consist of nested HTML elements.\r\n\r\nThis example contains 4 HTML elements:', 'html_home.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject1_menu`
--

CREATE TABLE `subject1_menu` (
  `id` int(11) NOT NULL,
  `subject_name` text NOT NULL,
  `subject_href` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject1_menu`
--

INSERT INTO `subject1_menu` (`id`, `subject_name`, `subject_href`) VALUES
(1, 'HTML Home', 'html.php'),
(2, 'Quiz', 'questions_subject1.php			 '),
(3, 'HTML Attributes', 'html_attributes.php'),
(4, 'HTML Styles', 'html_styles.php'),
(5, 'HTML Formatting', 'html_formatting.php'),
(6, 'HTML Color', 'html_color.php'),
(7, 'HTML 5 Tutorials', 'html5_tutorials.php');

-- --------------------------------------------------------

--
-- Table structure for table `subject2_elements`
--

CREATE TABLE `subject2_elements` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject2_elements`
--

INSERT INTO `subject2_elements` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'CSS Backgrounds\n', 'The CSS background properties are used to define the background effects for elements.\n\nCSS background properties:\n\nbackground-color\nbackground-image\nbackground-repeat\nbackground-attachment\nbackground-position', 'css-colors.png'),
(2, 'Background Color\n', 'The background-color property specifies the background color of an element.\n\nThe background color of a page is set like this:\n', 'css_back-color.png'),
(3, 'Background Image\r\n', '\r\nThe background-image property specifies an image to use as the background of an element.\r\n\r\nBy default, the image is repeated so it covers the entire element.\r\n\r\nThe background image for a page can be set like this:', 'css-back-image.png'),
(4, 'Background Image - Repeat Horizontally or Vertically\n', 'By default, the background-image property repeats an image both horizontally and vertically.\n\nSome images should be repeated only horizontally or vertically, or they will look strange, like this:                                     If the image above is repeated only horizontally (background-repeat: repeat-x;), the background will look better:\n', 'css-back-repeat.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject2_elements2`
--

CREATE TABLE `subject2_elements2` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(30000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject2_elements2`
--

INSERT INTO `subject2_elements2` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'Border Style\n', 'The border-style property specifies what kind of border to display.\n\nThe following values are allowed:\n\ndotted - Defines a dotted border\ndashed - Defines a dashed border\nsolid - Defines a solid border\ndouble - Defines a double border\ngroove - Defines a 3D grooved border. The effect depends on the border-color value\nridge - Defines a 3D ridged border. The effect depends on the border-color value\ninset - Defines a 3D inset border. The effect depends on the border-color value\noutset - Defines a 3D outset border. The effect depends on the border-color value\nnone - Defines no border\nhidden - Defines a hidden border\nThe border-style property can have from one to four values (for the top border, right border, bottom border, and the left border).', 'css-borders.png'),
(2, 'Border Width\r\n', 'The border-width property specifies the width of the four borders.\r\n\r\nThe width can be set as a specific size (in px, pt, cm, em, etc) or by using one of the three pre-defined values: thin, medium, or thick.\r\n\r\nThe border-width property can have from one to four values (for the top border, right border, bottom border, and the left border).', 'css-border-width.png'),
(3, 'Border Color\r\n', 'The border-color property is used to set the color of the four borders.\r\n\r\nThe color can be set by:\r\n\r\nname - specify a color name, like "red"\r\nHex - specify a hex value, like "#ff0000"\r\nRGB - specify a RGB value, like "rgb(255,0,0)"\r\ntransparent\r\nThe border-color property can have from one to four values (for the top border, right border, bottom border, and the left border). \r\n\r\nIf border-color is not set, it inherits the color of the element.', 'css-border-color.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject2_elements3`
--

CREATE TABLE `subject2_elements3` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject2_elements3`
--

INSERT INTO `subject2_elements3` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'Margin - Individual Sides\n', 'CSS has properties for specifying the margin for each side of an element:\n\nmargin-top\nmargin-right\nmargin-bottom\nmargin-left\nAll the margin properties can have the following values:\n\nauto - the browser calculates the margin\nlength - specifies a margin in px, pt, cm, etc.\n% - specifies a margin in % of the width of the containing element\ninherit - specifies that the margin should be inherited from the parent element', 'css-margins.png'),
(2, 'Margin - Shorthand Property\r\n', 'To shorten the code, it is possible to specify all the margin properties in one property.\r\n\r\nThe margin property is a shorthand property for the following individual margin properties:\r\n\r\nmargin-top\r\nmargin-right\r\nmargin-bottom\r\nmargin-left', 'css-margins.png'),
(3, 'Use of The auto Value\r\n', 'You can set the margin property to auto to horizontally center the element within its container.\r\n\r\nThe element will then take up the specified width, and the remaining space will be split equally between the left and right margins:', 'css-margins-auto.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject2_elements4`
--

CREATE TABLE `subject2_elements4` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject2_elements4`
--

INSERT INTO `subject2_elements4` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'Padding - Individual Sides\n', 'CSS has properties for specifying the padding for each side of an element:\n\npadding-top\npadding-right\npadding-bottom\npadding-left\nAll the padding properties can have the following values:\n\nlength - specifies a padding in px, pt, cm, etc.\n% - specifies a padding in % of the width of the containing element\ninherit - specifies that the padding should be inherited from the parent element\nThe following example sets different padding for all four sides of a <p> element: ', 'heading.png'),
(2, 'Padding - Shorthand Property\r\n', 'To shorten the code, it is possible to specify all the padding properties in one property.\r\n\r\nThe padding property is a shorthand property for the following individual padding properties:\r\n\r\npadding-top\r\npadding-right\r\npadding-bottom\r\npadding-left\r\n', 'css-padding.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject2_elements5`
--

CREATE TABLE `subject2_elements5` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `video_src` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject2_elements5`
--

INSERT INTO `subject2_elements5` (`id`, `name`, `value`, `video_src`) VALUES
(1, 'CSS Tutorial part 1', 'How to make a website: CSS Tutorial - Basics. This is the first in a series of videos designed to teach the basics of CSS. This video will show the basic structure of a CSS style and will show 3 different methods to apply styles. This tutorial is based on CSS version 2.1.', 'https://www.youtube-nocookie.com/embed/Wz2klMXDqF4'),
(2, 'CSS Tutorial Part 2', 'How to make a website: CSS Tutorial - Basics. This is the second in a series of videos designed to teach the basics of CSS. This video will demonstrate the universal, type, class, and ID selectors. This tutorial is based on CSS version 2.1.', 'https://www.youtube-nocookie.com/embed/6rKan6loNTw'),
(3, 'CSS Tutorial Part 3', 'How to make a website: CSS Tutorial - Basics. This is the second in a series of videos designed to teach the basics of CSS. This video will demonstrate the universal, type, class, and ID selectors. This tutorial is based on CSS version 2.1.', 'https://www.youtube-nocookie.com/embed/NR4arpSA2jI'),
(4, 'CSS Tutorials part 4', 'How to make a website: CSS Tutorial - Basics. This is the fourth in a series of videos designed to teach the basics of CSS. This video will demonstrate how to position elements. The static, relative, absolute, and fixed positioning methods will be covered. This tutorial is based on CSS version 2.1.', 'https://www.youtube-nocookie.com/embed/W5ycN9jBuBw');

-- --------------------------------------------------------

--
-- Table structure for table `subject2_home`
--

CREATE TABLE `subject2_home` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject2_home`
--

INSERT INTO `subject2_home` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'Welcome To CSS course', 'CSS is a stylesheet language that describes the presentation of an HTML (or XML) document.\r\n\r\nCSS describes how elements must be rendered on screen, on paper, or in other media.\r\n\r\nThis tutorial will teach you CSS from basic to advanced.', 'css_home.png'),
(2, 'CSS Syntax', 'A CSS rule-set consists of a selector and a declaration block:\r\n\r\nThe selector points to the HTML element you want to style.\r\n\r\nThe declaration block contains one or more declarations separated by semicolons.\r\n\r\nEach declaration includes a CSS property name and a value, separated by a colon.\r\n\r\nA CSS declaration always ends with a semicolon, and declaration blocks are surrounded by curly braces.\r\n\r\nIn the following example all <p> elements will be center-aligned, with a red text color:', 'css_syntax.png'),
(3, 'The element Selector', 'The element selector selects elements based on the element name.\r\n\r\nYou can select all <p> elements on a page like this (in this case, all <p> elements will be center-aligned, with a red text color):', 'css-selector.png'),
(4, 'The id Selector', '\r\nThe id selector uses the id attribute of an HTML element to select a specific element.\r\n\r\nThe id of an element should be unique within a page, so the id selector is used to select one unique element!\r\n\r\nTo select an element with a specific id, write a hash (#) character, followed by the id of the element.\r\n\r\nThe style rule below will be applied to the HTML element with id="para1":', 'css-id.png'),
(5, 'The class Selector', 'The class selector selects elements with a specific class attribute.\r\n\r\nTo select elements with a specific class, write a period (.) character, followed by the name of the class.\r\n\r\nIn the example below, all HTML elements with class="center" will be red and center-aligned:', 'css-class.png'),
(6, 'CSS Comments', '\r\nComments are used to explain the code, and may help when you edit the source code at a later date.\r\n\r\nComments are ignored by browsers.\r\n\r\nA CSS comment starts with /* and ends with */. Comments can also span multiple lines:', 'css-comments.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject2_menu`
--

CREATE TABLE `subject2_menu` (
  `id` int(11) NOT NULL,
  `subject_name` text NOT NULL,
  `subject_href` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject2_menu`
--

INSERT INTO `subject2_menu` (`id`, `subject_name`, `subject_href`) VALUES
(1, 'HOME', 'css.php'),
(2, 'CSS Backgrounds', 'css_backgroung.php'),
(3, 'CSS Quiz', 'questions_subject2.php'),
(4, 'CSS Borders', 'css-borders.php'),
(5, 'CSS Margins', 'css_margins.php'),
(6, 'CSS Padding', 'css_padding.php'),
(7, 'CSS Tutorials ', 'css_tutorials.php');

-- --------------------------------------------------------

--
-- Table structure for table `subject3_elements`
--

CREATE TABLE `subject3_elements` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject3_elements`
--

INSERT INTO `subject3_elements` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'JavaScript Assignment Operators\r\n', 'Assignment operators assign values to JavaScript variables.\r\n', 'js-assignment-operators.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject3_elements2`
--

CREATE TABLE `subject3_elements2` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject3_elements2`
--

INSERT INTO `subject3_elements2` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'The Math Object\r\n', 'The Math object allows you to perform mathematical tasks.\r\n\r\nThe Math object includes several mathematical methods.\r\n\r\nOne common use of the Math object is to create a random number:', 'js-math-random.png'),
(2, 'Math.min() and Math.max()\r\n', 'Math.min() and Math.max() can be used to find the lowest or highest value in a list of arguments:\r\n', 'js-math-min.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject3_elements3`
--

CREATE TABLE `subject3_elements3` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject3_elements3`
--

INSERT INTO `subject3_elements3` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'Displaying Arrays\r\n', 'In this tutorial we will use a script to display arrays inside a <p> element with id="demo":\r\n\r\n<p id="demo"></p>\r\n\r\n<script>\r\nvar cars = ["Saab", "Volvo", "BMW"];\r\ndocument.getElementById("demo").innerHTML = cars;\r\n</script>', 'js-arrays.png'),
(2, 'Creating an Array\r\n', 'Using an array literal is the easiest way to create a JavaScript Array.\r\n\r\nSyntax:\r\n\r\nvar array-name = [item1, item2, ...];       \r\nExample:\r\n\r\nvar cars = ["Saab", "Volvo", "BMW"];', 'js-arrays.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject3_elements4`
--

CREATE TABLE `subject3_elements4` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject3_elements4`
--

INSERT INTO `subject3_elements4` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'Boolean Values\r\n', 'Very often, in programming, you will need a data type that can only have one of two values, like\r\n\r\nYES / NO\r\nON / OFF\r\nTRUE / FALSE\r\nFor this, JavaScript has a Boolean data type. It can only take the values true or false.\r\n\r\nThe Boolean() Function\r\nYou can use the Boolean() function to find out if an expression (or a variable) is true:', 'js-boolean.png'),
(2, 'Comparisons and Conditions\r\n', 'The chapter JS Comparisons gives a full overview of comparison operators.\r\n\r\nThe chapter JS Conditions gives a full overview of conditional statements.\r\n\r\nHere are some examples:', 'js-boolean-op.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject3_elements5`
--

CREATE TABLE `subject3_elements5` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `video_src` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject3_elements5`
--

INSERT INTO `subject3_elements5` (`id`, `name`, `value`, `video_src`) VALUES
(1, 'Javascript Tutorial 1', '\nThis is part one of Javascript Programming. If you''ve been wanting to learn Javascript, this will help you get a quick head start. We''ll cover the very basics and get you prepared for part 2 which will teach you to use jQuery to do some fun stuff to your web page in real time!', 'https://www.youtube-nocookie.com/embed/vZBCTc9zHtI'),
(2, 'Javascript Tutorial 2', 'This is part 2 of Javascript programming and we''re going to get right into using jQuery for programming. jQuery makes it extremely easy and fun to code javascript. You can write more function with a lot less javascript code.', 'https://www.youtube-nocookie.com/embed/WGPaZNYrHpo');

-- --------------------------------------------------------

--
-- Table structure for table `subject3_home`
--

CREATE TABLE `subject3_home` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject3_home`
--

INSERT INTO `subject3_home` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'Javascript Tutorial', 'JavaScript is the programming language of HTML and the Web.\n\nProgramming makes computers do what you want them to do.\n\nJavaScript is easy to learn.\n\nThis tutorial will teach you JavaScript from basic to advanced.\n', 'js-home.png'),
(2, 'JavaScript Syntax\r\n', 'JavaScript Programs\r\nA computer program is a list of "instructions" to be "executed" by the computer.\r\n\r\nIn a programming language, these program instructions are called statements.\r\n\r\nJavaScript is a programming language.\r\n\r\nJavaScript statements are separated by semicolons.', 'js-statements.png'),
(3, 'Javascript Numbers', 'The most important rules for writing fixed values are:\r\n\r\nNumbers are written with or without decimals:', 'js-numbers.png'),
(4, 'Javascript Strings', 'Strings are text, written within double or single quotes:\r\n', 'js-strings.png'),
(5, 'Javascript Variables', 'In a programming language, variables are used to store data values.\r\n\r\nJavaScript uses the var keyword to declare variables.\r\n\r\nAn equal sign is used to assign values to variables.\r\n\r\nIn this example, x is defined as a variable. Then, x is assigned (given) the value 6:', 'js-variables.png'),
(6, 'Javascript Comments', 'Not all JavaScript statements are "executed".\r\n\r\nCode after double slashes // or between /* and */ is treated as a comment.\r\n\r\nComments are ignored, and will not be executed:', 'js-comments.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject3_menu`
--

CREATE TABLE `subject3_menu` (
  `id` int(11) NOT NULL,
  `subject3_menu_name` text NOT NULL,
  `subject3_menu_href` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject3_menu`
--

INSERT INTO `subject3_menu` (`id`, `subject3_menu_name`, `subject3_menu_href`) VALUES
(1, 'JS Home', 'js_home.php'),
(2, 'JavaScript Math Object', 'js_math_object.php'),
(3, 'JS Assignment Operator', 'js-assignment-operators.php'),
(4, 'JavaScript Arrays', 'js_arrays.php'),
(5, 'JavaScript Booleans', 'js-booleans.php'),
(6, 'JAVASCRIPT Tutorials', 'js-tutorials.php');

-- --------------------------------------------------------

--
-- Table structure for table `subject4_elements`
--

CREATE TABLE `subject4_elements` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject4_elements`
--

INSERT INTO `subject4_elements` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'PHP 5 Form Handling\r\n', 'The example below displays a simple HTML form with two input fields and a submit button:\r\n\r\nExample\r\n<html>\r\n<body>\r\n\r\n<form action="welcome.php" method="post">\r\nName: <input type="text" name="name"><br>\r\nE-mail: <input type="text" name="email"><br>\r\n<input type="submit">\r\n</form>\r\n\r\n</body>\r\n</html>\r\n\r\nWhen the user fills out the form above and clicks the submit button, the form data is sent for processing to a PHP file named "welcome.php". The form data is sent with the HTTP POST method.\r\n\r\nTo display the submitted data you could simply echo all the variables. The "welcome.php" looks like this:', 'php-forms.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject4_elements2`
--

CREATE TABLE `subject4_elements2` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject4_elements2`
--

INSERT INTO `subject4_elements2` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'PHP - Two-dimensional Arrays\r\n', 'A two-dimensional array is an array of arrays (a three-dimensional array is an array of arrays of arrays).\r\n\r\nFirst, take a look at the following table:', 'php-arrays.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject4_elements3`
--

CREATE TABLE `subject4_elements3` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject4_elements3`
--

INSERT INTO `subject4_elements3` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'PHP MySQL Database\r\n', 'With PHP, you can connect to and manipulate databases.\r\n\r\nMySQL is the most popular database system used with PHP.\r\n\r\nWhat is MySQL?\r\nMySQL is a database system used on the web\r\nMySQL is a database system that runs on a server\r\nMySQL is ideal for both small and large applications\r\nMySQL is very fast, reliable, and easy to use\r\nMySQL uses standard SQL\r\nMySQL compiles on a number of platforms\r\nMySQL is free to download and use\r\nMySQL is developed, distributed, and supported by Oracle Corporation\r\nMySQL is named after co-founder Monty Widenius''s daughter: My\r\nThe data in a MySQL database are stored in tables. A table is a collection of related data, and it consists of columns and rows.\r\n\r\nDatabases are useful for storing information categorically. A company may have a database with the following tables:\r\n\r\nEmployees\r\nProducts\r\nCustomers\r\nOrders\r\nPHP + MySQL Database System\r\nPHP combined with MySQL are cross-platform (you can develop in Windows and serve on a Unix platform)\r\nDatabase Queries\r\nA query is a question or a request.\r\n\r\nWe can query a database for specific information and have a recordset returned.\r\n\r\nLook at the following query (using standard SQL):\r\n\r\nSELECT LastName FROM Employees\r\nThe query above selects all the data in the "LastName" column from the "Employees" table.\r\n\r\nTo learn more about SQL, please visit our SQL tutorial.\r\n\r\nDownload MySQL Database\r\nIf you don''t have a PHP server with a MySQL Database, you can download it for free here: http://www.mysql.com\r\nFacts About MySQL Database\r\nMySQL is the de-facto standard database system for web sites with HUGE volumes of both data and end-users (like Facebook, Twitter, and Wikipedia).\r\n\r\nAnother great thing about MySQL is that it can be scaled down to support embedded database applications.', 'php-mysql.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject4_elements4`
--

CREATE TABLE `subject4_elements4` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject4_elements4`
--

INSERT INTO `subject4_elements4` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'PHP XML Parsers\r\n', 'What is XML?\r\nThe XML language is a way to structure data for sharing across websites.\r\n\r\nSeveral web technologies like RSS Feeds and Podcasts are written in XML.\r\n\r\nXML is easy to create. It looks a lot like HTML, except that you make up your own tags.\r\n\r\nIf you want to learn more about XML, please visit our XML tutorial.\r\n\r\nWhat is an XML Parser?\r\nTo read and update, create and manipulate an XML document, you will need an XML parser.\r\n\r\nIn PHP there are two major types of XML parsers:\r\n\r\nTree-Based Parsers\r\nEvent-Based Parsers\r\nTree-Based Parsers\r\nTree-based parsers holds the entire document in Memory and transforms the XML document into a Tree structure. It analyzes the whole document, and provides access to the Tree elements (DOM).\r\n\r\nThis type of parser is a better option for smaller XML documents, but not for large XML document as it causes major performance issues.\r\n\r\nExample of tree-based parsers:\r\n\r\nSimpleXML\r\nDOM\r\nEvent-Based Parsers\r\nEvent-based parsers do not hold the entire document in Memory, instead, they read in one node at a time and allow you to interact with in real time. Once you move onto the next node, the old one is thrown away.\r\n\r\nThis type of parser is well suited for large XML documents. It parses faster and consumes less memory.\r\n\r\nExample of event-based parsers:\r\n\r\nXMLReader\r\nXML Expat Parser', 'php-xml.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject4_elements5`
--

CREATE TABLE `subject4_elements5` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `video_src` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject4_elements5`
--

INSERT INTO `subject4_elements5` (`id`, `name`, `value`, `video_src`) VALUES
(1, 'PHP Tutorial 1', 'In this tutorial I will cover the basics such as what is PHP, some past, present and future notes on PHP and what it''s used for as well as how it works. We''ll also touch on a few topics to be discussed in further detail in the coming tutorials. Please feel free to leave your comments, questions, suggestions.', 'https://www.youtube-nocookie.com/embed/7UMNz5T-0VQ');

-- --------------------------------------------------------

--
-- Table structure for table `subject4_home`
--

CREATE TABLE `subject4_home` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject4_home`
--

INSERT INTO `subject4_home` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'PHP 5 Tutorial\r\n', 'PHP is a server scripting language, and a powerful tool for making dynamic and interactive Web pages.\r\n\r\nPHP is a widely-used, free, and efficient alternative to competitors such as Microsoft''s ASP.', 'php-home.png'),
(2, 'PHP 5 Introduction\r\n', 'What You Should Already Know\r\nBefore you continue you should have a basic understanding of the following:\r\n\r\nHTML\r\nCSS\r\nJavaScript\r\nIf you want to study these subjects first, find the tutorials on our Home page.\r\n\r\nWhat is PHP?\r\nPHP is an acronym for "PHP: Hypertext Preprocessor"\r\nPHP is a widely-used, open source scripting language\r\nPHP scripts are executed on the server\r\nPHP is free to download and use\r\nNote	PHP is an amazing and popular language!\r\n\r\nIt is powerful enough to be at the core of the biggest blogging system on the web (WordPress)!\r\nIt is deep enough to run the largest social network (Facebook)!\r\nIt is also easy enough to be a beginner''s first server side language!\r\nWhat is a PHP File?\r\nPHP files can contain text, HTML, CSS, JavaScript, and PHP code\r\nPHP code are executed on the server, and the result is returned to the browser as plain HTML\r\nPHP files have extension ".php"\r\nWhat Can PHP Do?\r\nPHP can generate dynamic page content\r\nPHP can create, open, read, write, delete, and close files on the server\r\nPHP can collect form data\r\nPHP can send and receive cookies\r\nPHP can add, delete, modify data in your database\r\nPHP can be used to control user-access\r\nPHP can encrypt data\r\nWith PHP you are not limited to output HTML. You can output images, PDF files, and even Flash movies. You can also output any text, such as XHTML and XML.\r\n\r\nWhy PHP?\r\nPHP runs on various platforms (Windows, Linux, Unix, Mac OS X, etc.)\r\nPHP is compatible with almost all servers used today (Apache, IIS, etc.)\r\nPHP supports a wide range of databases\r\nPHP is free. Download it from the official PHP resource: www.php.net\r\nPHP is easy to learn and runs efficiently on the server side\r\n', 'php-intro.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject4_menu`
--

CREATE TABLE `subject4_menu` (
  `id` int(11) NOT NULL,
  `subject4_menu_name` text NOT NULL,
  `subject4_menu_href` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject4_menu`
--

INSERT INTO `subject4_menu` (`id`, `subject4_menu_name`, `subject4_menu_href`) VALUES
(1, 'PHP Home', 'php_course.php'),
(2, 'PHP Forms', 'php-forms.php'),
(3, 'PHP Arrays', 'php-arrays.php'),
(4, 'PHP MySQL Database', 'php-mysql.php'),
(5, 'PHP XML Parser', 'php-xml.php'),
(6, 'PHP Tutorials', 'php-tutorials.php');

-- --------------------------------------------------------

--
-- Table structure for table `subject5_elements`
--

CREATE TABLE `subject5_elements` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject5_elements`
--

INSERT INTO `subject5_elements` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'SQL Functions', 'SQL Aggregate Functions\r\nSQL aggregate functions return a single value, calculated from values in a column.\r\n\r\nUseful aggregate functions:\r\n\r\nAVG() - Returns the average value\r\nCOUNT() - Returns the number of rows\r\nFIRST() - Returns the first value\r\nLAST() - Returns the last value\r\nMAX() - Returns the largest value\r\nMIN() - Returns the smallest value\r\nSUM() - Returns the sum\r\nSQL Scalar functions\r\nSQL scalar functions return a single value, based on the input value.\r\n\r\nUseful scalar functions:\r\n\r\nUCASE() - Converts a field to upper case\r\nLCASE() - Converts a field to lower case\r\nMID() - Extract characters from a text field\r\nLEN() - Returns the length of a text field\r\nROUND() - Rounds a numeric field to the number of decimals specified\r\nNOW() - Returns the current system date and time\r\nFORMAT() - Formats how a field is to be displayed\r\n', 'sql-fun.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject5_elements2`
--

CREATE TABLE `subject5_elements2` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject5_elements2`
--

INSERT INTO `subject5_elements2` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'SQL Injection', 'SQL in Web Pages\r\nIn the previous chapters, you have learned to retrieve (and update) database data, using SQL.\r\n\r\nWhen SQL is used to display data on a web page, it is common to let web users input their own search values.\r\n\r\nSince SQL statements are text only, it is easy, with a little piece of computer code, to dynamically change SQL statements to provide the user with selected data:', 'sql-injection.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject5_elements3`
--

CREATE TABLE `subject5_elements3` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject5_elements3`
--

INSERT INTO `subject5_elements3` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'SQL UPDATE Statement\r\n', 'The SQL UPDATE Statement\r\nThe UPDATE statement is used to update existing records in a table.\r\n\r\nSQL UPDATE Syntax\r\nUPDATE table_name\r\nSET column1=value1,column2=value2,...\r\nWHERE some_column=some_value;', 'sql-update.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject5_elements4`
--

CREATE TABLE `subject5_elements4` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject5_elements4`
--

INSERT INTO `subject5_elements4` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'SQL INSERT INTO Statement\r\n', 'The SQL INSERT INTO Statement\r\nThe INSERT INTO statement is used to insert new records in a table.\r\n\r\nSQL INSERT INTO Syntax\r\nIt is possible to write the INSERT INTO statement in two forms.\r\n\r\nThe first form does not specify the column names where the data will be inserted, only their values:\r\n\r\nINSERT INTO table_name\r\nVALUES (value1,value2,value3,...);\r\nThe second form specifies both the column names and the values to be inserted:\r\n\r\nINSERT INTO table_name (column1,column2,column3,...)\r\nVALUES (value1,value2,value3,...);', 'sql-insert.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject5_elements5`
--

CREATE TABLE `subject5_elements5` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `video_src` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject5_elements5`
--

INSERT INTO `subject5_elements5` (`id`, `name`, `value`, `video_src`) VALUES
(1, 'SQL Tutorial 1', 'A fun, energetic and clear introduction to SQL and databases for just about anybody to understand! Finally understand SQL without being bored to tears or getting frustrated. Also grab my nice DISCOUNT on my SQL for FinTech job seekers course: https://www.udemy.com/sql-database-co... This video answers:\r\n\r\nWhat is SQL?\r\nHow is SQL used?\r\nWhat is a database?\r\nWhy use a database instead of a spreadsheet?', 'https://www.youtube-nocookie.com/embed/HgoM1I4yEFo');

-- --------------------------------------------------------

--
-- Table structure for table `subject5_home`
--

CREATE TABLE `subject5_home` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` varchar(3000) NOT NULL,
  `image_src` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject5_home`
--

INSERT INTO `subject5_home` (`id`, `name`, `value`, `image_src`) VALUES
(1, 'SQL Tutorial\r\n', 'SQL is a standard language for accessing databases.\r\n\r\nOur SQL tutorial will teach you how to use SQL to access and manipulate data in: MySQL, SQL Server, Access, Oracle, Sybase, DB2, and other database systems.\r\n\r\nSELECT * FROM Customers;\r\n', 'sql-home.png');

-- --------------------------------------------------------

--
-- Table structure for table `subject5_menu`
--

CREATE TABLE `subject5_menu` (
  `id` int(11) NOT NULL,
  `subject5_menu_name` text NOT NULL,
  `subject5_menu_href` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject5_menu`
--

INSERT INTO `subject5_menu` (`id`, `subject5_menu_name`, `subject5_menu_href`) VALUES
(1, 'SQL HOME', 'sqlcourse.php'),
(2, 'SQL Functions', 'sql_functions.php'),
(3, 'SQL Injection', 'sql_injection.php'),
(4, 'SQL Update', 'sql_update.php'),
(5, 'SQL Insert', 'sql_insert.php'),
(6, 'SQL Tutorials', 'sql_tutorials.php');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `admin` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `firstname`, `lastname`, `admin`) VALUES
(3, 'pambos', 'fcea920f7412b5da7be0', 'pambos', 'papachristofi', 1),
(5, 'pambos123', 'e86fdc2283aff4717103f2d44d0610f7', 'pambos33', 'papachristofi33', 1),
(8, 'alex', 'b75bd008d5fecb1f50cf026532e8ae67', 'alex', 'alex', 0),
(9, 'alex123', 'fcea920f7412b5da7be0cf42b8c93759', 'alex', 'alex', 0),
(10, 'pambos444', 'e86fdc2283aff4717103f2d44d0610f7', 'alex', 'alex', 0),
(11, 'pambos4444', 'dcb64c94e1b81cd1cd3eb4a73ad27d99', 'pambos', 'papachristofi', 1),
(12, 'samsteel', 'de00696cd069d4e181345605a97292c3', 'Sam', 'Steel', 0);

-- --------------------------------------------------------

--
-- Table structure for table `web_name`
--

CREATE TABLE `web_name` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_name`
--

INSERT INTO `web_name` (`id`, `name`) VALUES
(1, 'Online Learning');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answers_subject1`
--
ALTER TABLE `answers_subject1`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `answers_subject2`
--
ALTER TABLE `answers_subject2`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `css_menu`
--
ALTER TABLE `css_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `questions2`
--
ALTER TABLE `questions2`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `subject1_elements`
--
ALTER TABLE `subject1_elements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject1_elements2`
--
ALTER TABLE `subject1_elements2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject1_elements3`
--
ALTER TABLE `subject1_elements3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject1_elements4`
--
ALTER TABLE `subject1_elements4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject1_elements5`
--
ALTER TABLE `subject1_elements5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject1_home`
--
ALTER TABLE `subject1_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject1_menu`
--
ALTER TABLE `subject1_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject2_elements`
--
ALTER TABLE `subject2_elements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject2_elements2`
--
ALTER TABLE `subject2_elements2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject2_elements3`
--
ALTER TABLE `subject2_elements3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject2_elements4`
--
ALTER TABLE `subject2_elements4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject2_elements5`
--
ALTER TABLE `subject2_elements5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject2_home`
--
ALTER TABLE `subject2_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject2_menu`
--
ALTER TABLE `subject2_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject3_elements`
--
ALTER TABLE `subject3_elements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject3_elements2`
--
ALTER TABLE `subject3_elements2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject3_elements3`
--
ALTER TABLE `subject3_elements3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject3_elements4`
--
ALTER TABLE `subject3_elements4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject3_elements5`
--
ALTER TABLE `subject3_elements5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject3_home`
--
ALTER TABLE `subject3_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject3_menu`
--
ALTER TABLE `subject3_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject4_elements`
--
ALTER TABLE `subject4_elements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject4_elements2`
--
ALTER TABLE `subject4_elements2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject4_elements3`
--
ALTER TABLE `subject4_elements3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject4_elements4`
--
ALTER TABLE `subject4_elements4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject4_elements5`
--
ALTER TABLE `subject4_elements5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject4_home`
--
ALTER TABLE `subject4_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject4_menu`
--
ALTER TABLE `subject4_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject5_elements`
--
ALTER TABLE `subject5_elements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject5_elements2`
--
ALTER TABLE `subject5_elements2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject5_elements3`
--
ALTER TABLE `subject5_elements3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject5_elements4`
--
ALTER TABLE `subject5_elements4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject5_elements5`
--
ALTER TABLE `subject5_elements5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject5_home`
--
ALTER TABLE `subject5_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject5_menu`
--
ALTER TABLE `subject5_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `web_name`
--
ALTER TABLE `web_name`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `answers_subject1`
--
ALTER TABLE `answers_subject1`
  MODIFY `q_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `subject1_elements2`
--
ALTER TABLE `subject1_elements2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subject1_elements3`
--
ALTER TABLE `subject1_elements3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `subject1_elements4`
--
ALTER TABLE `subject1_elements4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subject1_elements5`
--
ALTER TABLE `subject1_elements5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subject1_home`
--
ALTER TABLE `subject1_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `subject1_menu`
--
ALTER TABLE `subject1_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subject2_elements2`
--
ALTER TABLE `subject2_elements2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subject2_elements3`
--
ALTER TABLE `subject2_elements3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subject2_elements4`
--
ALTER TABLE `subject2_elements4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subject2_elements5`
--
ALTER TABLE `subject2_elements5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `subject2_home`
--
ALTER TABLE `subject2_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subject2_menu`
--
ALTER TABLE `subject2_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subject3_elements`
--
ALTER TABLE `subject3_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject3_elements2`
--
ALTER TABLE `subject3_elements2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subject3_elements3`
--
ALTER TABLE `subject3_elements3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subject3_elements4`
--
ALTER TABLE `subject3_elements4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subject3_elements5`
--
ALTER TABLE `subject3_elements5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subject3_home`
--
ALTER TABLE `subject3_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subject3_menu`
--
ALTER TABLE `subject3_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subject4_elements`
--
ALTER TABLE `subject4_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject4_elements2`
--
ALTER TABLE `subject4_elements2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject4_elements3`
--
ALTER TABLE `subject4_elements3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject4_elements4`
--
ALTER TABLE `subject4_elements4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject4_elements5`
--
ALTER TABLE `subject4_elements5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject4_home`
--
ALTER TABLE `subject4_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subject4_menu`
--
ALTER TABLE `subject4_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subject5_elements`
--
ALTER TABLE `subject5_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject5_elements2`
--
ALTER TABLE `subject5_elements2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject5_elements3`
--
ALTER TABLE `subject5_elements3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject5_elements4`
--
ALTER TABLE `subject5_elements4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject5_elements5`
--
ALTER TABLE `subject5_elements5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject5_home`
--
ALTER TABLE `subject5_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subject5_menu`
--
ALTER TABLE `subject5_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `web_name`
--
ALTER TABLE `web_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
