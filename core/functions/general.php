<!-- The source code used for this project is open source and it is only used for reseacrh and the final year project purposes. -->

<?php
//redirect to home.php if user is logged in and tries to access register.php
function logged_in_redirect(){
	if(logged_in()===true){
		header('Location:home.php');
		exit();
	}
}
//protect pages courses.php, forum.php for no access if user
// is not logged in
function protect_page(){
	if(logged_in()===false){
		header('Location:protected.php');
		exit();
	}
}

function admin_protect(){
	global $user_data;
	if (is_admin($user_data['user_id']) == false) {
		header('Location:admin.php');
		exit();
	}

}
function array_sazinitize($item){
	$item = htmlentities(strip_tags(mysql_real_escape_string($item)));
}
function sanitize ($data){
	return htmlentities(strip_tags(mysql_real_escape_string($data)));

}
function output_errors($errors){

	return '<ul><li>'.implode('</li><li>',$errors) .'</li></ul>';

}

?>
