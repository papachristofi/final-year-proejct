
<?php
include 'core/init.php';
logged_in_redirect();
include 'includes/overall/overall_header.php';
if(empty($_POST) ===false){
	$required_fields = array('username','password','password_again','firstname','lastname');
	foreach($_POST as $key=>$value){
		if(empty($value) && in_array($key,$required_fields) ===true){
			$errors[] = 'Fields marked with an asterisk* are required';
			break 1;
		}
	}
	if(empty($errors)===true){
		if(user_exists($_POST['username'])===true){
			$errors[] = 'Sorry, the username \'' . $_POST['username']. '\' is already taken';
		}
		//regular expression to check for username spaces
		if(preg_match("/\\s/",$_POST['username']) == true){
			$errors[] = 'Your username must not contain any spaces!';

		}
		if(strlen($_POST['password']) <= 6){
				$errors[] ='Your passord must be at least 6 characters';
		}
		if($_POST['password'] !== $_POST['password_again']){
			$errors [] ='Your password do not match';
		}
	}
}
?>
<?php
if(isset($_GET['success']) && empty($_GET['success'])){
	echo "<h3> You\'ve been registered successfully </h3>";
}
else{
		if(empty($_POST)===false && empty($errors)===true){
			//register
			$register_data = array(
			'username'	=> 	$_POST['username'],
			'password'	=> 	$_POST['password'],
			'firstname'	=> 	$_POST['firstname'],
			'lastname'	=> 	$_POST['lastname']

			);
			array_sazinitize($register_data);
			register_user($register_data);
			echo '<script>window.location = "'.'register.php?success'.'";</script>';
			exit();
		}
		else if(empty($errors)===false){
			//errors print
			echo "<h3>" .output_errors($errors)."</h3>";
		}



?>



	<form role="form" action="register.php" method="post">

			<h2>Please Register <small>Fill All Fields With * </small></h2>

			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
            <input type="text" name="firstname" class="form-control input-lg" placeholder="First Name *" tabindex="1">
					</div>
				</div>
			</div>
			<div class="row">

				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="lastname"  class="form-control input-lg" placeholder="Last Name *" tabindex="2">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
			<div class="form-group">
				<input type="text" name="username"  class="form-control input-lg" placeholder="Username *" tabindex="3">
			</div>
		</div>

			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password *" tabindex="5">
					</div>
				</div>
			</div>
				<div class="row">

				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="password" name="password_again" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password *" tabindex="6">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-md-6"><input type="submit" value="Register" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
			</div>
		</form>


<?php
}
include 'includes/overall/overall_footer.php';
 ?>
