
<?php
include 'core/init.php';
protect_page();
admin_protect();
include 'includes/overall/overallheader_admin.php';?>

<h1>Subject4 Admin Page</h1>
<ol class="breadcrumb">
  <li><a href="admin.php">Admin</a></li>
  <li class="active">Subject4 Admin Page</li>
</ol>

<div class="list-group">
  <a href="subject4_home_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 4 Home</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 4 Home Details</p>
  </a>
</div>
<div class="list-group">
  <a href="subject4_content1_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 4 Data 1</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 4 Data 1 Details</p>
  </a>
</div>
<div class="list-group">
  <a href="subject4_content2_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 4 Data 2</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 4 Data 2 Details</p>
  </a>
</div>
<div class="list-group">
  <a href="subject4_content3_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 4 Data 3</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 4 Data 3 Details</p>
  </a>

</div>
<div class="list-group">
  <a href="subject4_content4_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 4 Data 4</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 4 Data 4 Details</p>
  </a>

</div>
<div class="list-group">
  <a href="subject4_content5_rename.php" class="list-group-item active">
    <h4 class="list-group-item-heading">Subject 4 Data 5</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 4 Data 5 Details</p>
  </a>
</div>
<div class="list-group">
  <a href="quiz4_questions.php" class="list-group-item secondary">
    <h4 class="list-group-item-heading">Subject 4 Quiz</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 4 Quiz Database</p>
  </a>

</div>
<div class="list-group">
  <a href="rename_submenu4.php" class="list-group-item secondary">
    <h4 class="list-group-item-heading">Rename Submenu 4</h4>
    <p class="list-group-item-text"><span class="glyphicon glyphicon-edit" aria-hidden="true">
    </span>Edit Subject 4 Submenu</p>
  </a>

</div>
<br><br><br>


<?php  include 'includes/overall/overall_footer.php';?>
