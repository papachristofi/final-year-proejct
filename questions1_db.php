
<?php
include 'core/init.php';
protect_page();
admin_protect();
include 'includes/overall/overallheader_admin.php';?>
<h3>Quiz 1 Questions</h3>

<a class="btn btn-default" href="quiz1_questions.php">Go back</a>
<a class="btn btn-default" href="add1_questions.php">Add</a>
<!-- <a class="btn btn-default" href="delete_sub1_cont1.php">Delete</a>
<a class="btn btn-default" href="update_sub1_cont1.php">Update</a> -->

<br>
<br>
<script>
$(document).ready(function() {
    var activeSystemClass = $('.list-group-item.active');

    //something is entered in search form
    $('#system-search').keyup( function() {
       var that = this;
        // affect all table rows on in systems table
        var tableBody = $('.table-list-search tbody');
        var tableRowsClass = $('.table-list-search tbody tr');
        $('.search-sf').remove();
        tableRowsClass.each( function(i, val) {

            //Lower text for case insensitive
            var rowText = $(val).text().toLowerCase();
            var inputText = $(that).val().toLowerCase();
            if(inputText != '')
            {
                $('.search-query-sf').remove();
                tableBody.prepend('<tr class="search-query-sf"><td colspan="6"><strong>Searching for: "'
                    + $(that).val()
                    + '"</strong></td></tr>');
            }
            else
            {
                $('.search-query-sf').remove();
            }

            if( rowText.indexOf( inputText ) == -1 )
            {
                //hide rows
                tableRowsClass.eq(i).hide();

            }
            else
            {
                $('.search-sf').remove();
                tableRowsClass.eq(i).show();
            }
        });
        //all tr elements are hidden
        if(tableRowsClass.children(':visible').length == 0)
        {
            tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="6">No entries found.</td></tr>');
        }
    });
});
</script>
<script>
$(document).ready(function(){
$("#mytable #checkall").click(function () {
        if ($("#mytable #checkall").is(':checked')) {
            $("#mytable input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("#mytable input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    });

    $("[data-toggle=tooltip]").tooltip();
});

$('#myModal').on('show', function() {
  var tit = $('.confirm-delete').data('title');

  $('#myModal .modal-body p').html("Desea eliminar al usuario " + '<b>' + tit +'</b>' + ' ?');
  var id = $(this).data('id'),
  removeBtn = $(this).find('.danger');
})

$('.confirm-delete').on('click', function(e) {
  e.preventDefault();

  var id = $(this).data('id');
  $('#myModal').data('id', id).modal('show');
});

$('#btnYes').click(function() {
  // handle deletion here
  var id = $('#myModal').data('id');
  $('[data-id='+id+']').parents('tr').remove();
  $('#myModal').modal('hide');

});

</script>
<form action="#" method="get">
    <div class="input-group">
        <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
        <input class="form-control" id="system-search" name="q" placeholder="Search for" required>
        <span class="input-group-btn">
            <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
        </span>
    </div>
</form>
<br>

<br>



<?php
$query = "SELECT * FROM questions"; //You don't need a ; like you do in SQL
$result = mysql_query($query);
echo "<div class='table-responsive'>";

echo "<table class='table table-list-search table-bordred table-striped'>"; // start a table tag in the HTML
?>


<thead>

<th><input type="checkbox" id="checkall" /></th>
<th>id</th>
 <th>Name</th>
  <th>Question Answer 1</th>
  <th>Question Answer 2</th>
  <th>Question Answer 3</th>
   <th>Edit</th>
    <th>Delete</th>
</thead>

<?php
while($row = mysql_fetch_row($result))
{
  echo "<tbody>";
echo "<tr>";
echo "<td> <input type='checkbox' class='checkthis' /></td>";
echo "<td>" . $row[0] . "</td>";
echo "<td>" . htmlentities($row[1]) . "</td>";
echo "<td>" . htmlentities($row[2]) . "</td>";
echo "<td>" . htmlentities($row[3]) . "</td>";
echo "<td>" . htmlentities($row[4]) . "</td>";
echo '<td><a class="btn mini blue-stripe" href="update1_questions.php?id=' . $row['0'] . '">Update</a></td>';
echo '<td><a href="delete1_questions.php?id=' . $row['0'] . '" onclick="return checkDelete()" class="confirm-delete btn mini red-stripe" role="button" data-title="'.$row[0].'" data-id="'.$row[0].'">Delete</a></td>';
echo "</tr>";
}
echo "</tbody>";

echo "</table>"; //Close the table in HTML
?>
</div>
<br>
<br>
<br>


<?php  include 'includes/overall/overall_footer.php';?>
