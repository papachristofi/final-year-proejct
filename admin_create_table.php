
<?php
include 'core/init.php';
protect_page();
admin_protect();
include 'includes/overall/overall_header.php';?>
<h4>Select the number of the fields to generate the table</h4>
<form action="admin_create_table.php" method="post">
  <label>Number of Fields: </label>
  <select name="num_fields">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
  </select>
  <br>
  <input class="btn btn-default" type="submit" name="submit" value="Submit" />
</form>
<?php
if(isset($_POST['submit'])){
$selected = $_POST['num_fields'];
echo "<form action='admin_create_table.php' method='post'>";
echo "<ul>";
echo "<li>";
echo "<label> Table Name: </label><br>";
echo "<input type='text' name='tbl_name' placeholder='Table Name'>";
for ($i=1; $i<=$selected; $i++) {
  echo"<li>";
    echo "<label>Field $i</label><br>";
    echo "<input type='text' name='field$i' placeholder='Field $i'>";
    echo "<select name='field_value$i'>";
      echo "<option value='INT'>INT</option>";
      echo "<option value='VARCHAR'>VARCHAR</option>";
      echo "<option value='TEXT'>TEXT</option>";
      echo "<option value='LONGTEXT'>LONGTEXT</option>";
      echo "<option value='DATE'>DATE</option>";
    echo "</select>";
  echo "</li><br>";
}
echo "</li>";
echo "</ul>";
echo "</form>";
}
echo "<input class='btn btn-default' type='submit' name='submit2' value='Submit' />";


if(isset($_POST['submit2']) && !empty($_POST['submit2'])){
  $table_name= $_POST['tbl_name'];
  echo $table_name;
  for ($i=1; $i<=$selected ; $i++) {
    $table_value=$_POST['field_value$i'];
    echo $table_value;
  }
  $table_field1=$_POST['field_value1'];
  echo $table_field1;
}
// $sql = "CREATE TABLE tutorials_tbl( ".
//        "tutorial_id INT NOT NULL AUTO_INCREMENT, ".
//        "tutorial_title VARCHAR(100) NOT NULL, ".
//        "tutorial_author VARCHAR(40) NOT NULL, ".
//        "submission_date DATE, ".
//        "PRIMARY KEY ( tutorial_id )); ";
?>



<?php  include 'includes/overall/overall_footer.php';?>

<!--
<ul>
  <li>
    <label>Table Name: </label><br>
    <input type='text' name='q_id' placeholder="Table Name">
  </li><br>
  <li>
    <label>Field 1</label><br>
    <input type='text' name='field1' placeholder="Field 1">
  </li><br>
  <li>
    <label>Field 2</label><br>
    <input type='text' name='field2' placeholder="Field 2">
  </li><br>
  <li>
    <label>Field 3</label><br>
    <input type='text' name='field3' placeholder="Field 3">
  </li><br>
  <li>
    <label>Field 1</label><br>
    <input type='text' name='field1' placeholder="Field 1">
  </li><br>
</ul> -->
