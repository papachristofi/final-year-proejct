<?php
include 'core/init.php';
logged_in_redirect();
if(empty($_POST) === false){
	$username = $_POST['username'];
	$password = $_POST['password'];
	
	
	if (empty($username) === true || empty($password) ===true ){
		$errors[] = 'you need to need to enter a username and a password';
	}
	else if (user_exists($username)=== false){
		$errors[] = ' we can\'t find that username. Have you Registered?';
	}
	else{
		
		if(strlen($password)>32){
			$errors[] = 'Password Too Long';
		}
		$login = login($username, $password);
		if($login ===false){
			$errors[]='That username/password combination is incorrect';
		}
		else{
			$_SESSION['user_id'] =$login;
			header('location: home.php');
			exit();
			
		}
	}
}else{
	$errors[]='No data received';
}
include 'includes/overall/overall_header.php';
if(empty($errors)===false){
?>
<h2>We tried to log you in, but...</h2>
<?php
}
echo output_errors($errors);

include 'includes/overall/overall_footer.php';


?>